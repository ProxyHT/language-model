import re
from nltk.lm import MLE
from nltk.lm.models import InterpolatedLanguageModel,Laplace,KneserNeyInterpolated
import time

start_time = time.time()

ngrams = 2

# lm = MLE(ngrams)
# lm = KneserNeyInterpolated(ngrams)
lm = Laplace(ngrams)

# TRAIN FILE, VALIDATION FILE AND TEST FILE
train_file = "data/train-80.txt"
val_file = "data/val-10.txt"
test_file = "data/test-20.txt"

train_file = "data/train-8000.txt"
val_file = "data/val-101.txt"
test_file = "data/test-2000.txt"
###


# GENERATE VOCABULARY FROM TRAIN FILE
ftrain = open(train_file,"r",encoding="utf-8")
vocab = []
from nltk.lm.preprocessing import pad_both_ends
print("Generating vocab")
count = 0
while True:
    count += 1
    line = ftrain.readline()
    if not line:
        break
    line = re.sub("\s+"," ",line)
    line = re.sub("\n","",line)
    line = line.lower()
    sentences = line.split(".")
    for sentence in sentences:
        vocab += [word for word in sentence.split(" ")]
# print(vocab)
print("Done vocab")
###


# TRAIN NGRAMS
ftrain = open(train_file,"r",encoding="utf-8")
from nltk.util import everygrams,bigrams
from nltk.lm.preprocessing import pad_both_ends

# dummy feed to pass vocab in
lm.fit([[("<UNK>","<UNK>")]], vocabulary_text=vocab)

# READ DATA FROM VALIDATION FILE
fval = open(val_file,"r",encoding="utf-8")
content = fval.read()
content = re.sub("\s+"," ",content)
content = re.sub("\n","",content)
content = content.lower()
words = content.split(" ")
val_padded_bigrams = list(pad_both_ends(words, n=ngrams))
val_padded_bigrams = [bigram for bigram in val_padded_bigrams if (len(bigram) > 0)]
val_bigrams = list(bigrams(val_padded_bigrams))

count = 0
while True:
    count += 1
    print("Reading line {}".format(count))
    line = ftrain.readline()
    if not line:
        break
    line = re.sub("\s+"," ",line)
    line = re.sub("\n","",line)
    line = line.lower()
    words = line.split(" ")
    training_padded_bigrams = list(pad_both_ends(words, n=ngrams))
    training_padded_bigrams = [bigram for bigram in training_padded_bigrams if (len(bigram) > 0)]
    training_bigrams = list(bigrams(training_padded_bigrams))

    lm.fit([training_bigrams])

    print("Training perplexity: ",lm.perplexity(training_bigrams))
    if count % 5000 == 0:
        print("Validation perplexity: ",lm.perplexity(val_bigrams))
print("Done fitting")


ftest = open(test_file,"r",encoding="utf-8")
content = ftest.read()
content = re.sub("\s+"," ",content)
content = re.sub("\n","",content)
content = content.lower()
words = content.split(" ")

test_padded_bigrams = list(pad_both_ends(words, n=ngrams))
test_bigrams = list(bigrams(padded_bigrams))
test_padded_bigrams = [bigram for bigram in test_padded_bigrams if (len(bigram) > 0)]
test_bigrams = list(bigrams(padded_bigrams))

print("Calculating perplexity of len",len(test_bigrams))
print("Perplexity on test:",lm.perplexity(test_padded_bigrams))

print("Time taken: ",time.time()-start_time)